# GSDB

The Game Slayer Javascript Relational Database Management System

A relational database management system written in Javascript which can be run in-browser and load data from files and interact with data in-memory.