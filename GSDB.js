class GSDB
{

  constructor(tables)
  {
    /* `tables` should be an array with the following architecture:
    tables[0]["name"]; => a string representing the table name
    tables[0]["data"]; => a string of comma separated values,
      table rows delineated by a line break.
    */

    this["tables"] = [];
    this["db"] = this;
  
    for (var i = 0; i < tables.length; i++)
    {

      let table = tables[i];
      this.initTable(table);
      
    }

  }

  initTable(table)
  {
    let formattedTable = this.csvToObj(table.data);
    this.tables[table.name] = [];
    this.tables[table.name]["name"] = table.name;
    this.tables[table.name]["data"] = formattedTable.data;
    this.tables[table.name]["columns"] = formattedTable.columns;
  }

  csvToObj(csv)
  {
    let lines = csv.split("\n");

    let fields = lines[0].split(",");

    let obj = [];

    let columns = [];

    for (var i = 0; i < lines.length; i++)
    {

      let currentLine = lines[i].split(",");

      /* Ignore comment or misformatted lines */
      if(lines[i] !== null && lines[i] !== undefined && lines[i] != "" && lines[i].charAt(0) != "#" && currentLine.length == fields.length)
      { 

        obj[currentLine[0]] = [];

        for (var j = 0; j < fields.length; j++)
        {

          if (i == 0)
          {
            columns.push(currentLine[j]);
          }
          else
          {
            obj[currentLine[0]][fields[j]] = currentLine[j];
          }

        }

      }

    }

    let ret = [];
    ret["columns"] = columns;
    ret["data"] = obj;

    return ret;

  }

  insert(table, values)
  {

    let newRowID = 0;

    if(this.tables[table] !== undefined && this.tables[table].data.length >= 1)
    {

      newRowID = this.tables[table].data.length;

    }

    let row = [];
    row["id"] = newRowID;
    for (var i = 1; i < this.tables[table].columns.length; i++)
    {

      row[this.tables[table].columns[i]] = values[i - 1];

    }

    this.tables[table].data.push(row);

    return newRowID;

  }

  update(table, column, newValue, whereColumn, whereValue)
  {
    /* CAUTION! :: Only use on tables where the whereValue is
       unique -- otherwise it will update one random row. */
    let result = this.select(table, whereColumn, whereValue);

    if(result)
    {
      let row = result[0];
      this.tables[table].data[row.id][column] = newValue;
      return true;
    }
    else
    {
      console.log("Cannot update.");
      return false;
    }
  }

  create(tableName, columns)
  {
    let table = []
    table["name"] = tableName;
    table["data"] = "";

    for (var i = 0; i < columns.length; i++)
    {
      table["data"] += columns[i];
      if (i < (columns.length - 1))
      {
        table["data"] += ",";
      }
    }

    this.initTable(table);

    return true;

  }

  delete(table, id)
  {
    delete(this.tables[table].data[id]);
  }

  select(table, column, value, filter)
  {
    return this.selectAdv(table, column, "=", value, filter);
  }

  selectAll(table)
  {
    if (this.tables[table] !== undefined)
    {
      return this.tables[table].data;
    }
    else
    {
      return false;
    }
  }
  
  selectAdv(table, column, operator, value, filter)
  {
    let ret = [];

    if (this.tables[table] !== undefined && this.tables[table] !== null)
    {
    
      if (operator == "highest" || operator == "HIGHEST")
      {
        this.tables[table].data = this.sortASC(this.tables[table].data, column);

        ret = this.tables[table].data[(this.tables[table].data.length - 1)];

        return ret;
      }

      for (var i = 0; i < this.tables[table].data.length; i++)
      {

        let match = false;

        if (this.tables[table].data[i] !== undefined)
        {
          switch(true)
          {
            case(operator == "=" && this.tables[table].data[i][column] == value):
            case(operator == "!=" && this.tables[table].data[i][column] != value):
            case(operator == ">" && this.tables[table].data[i][column] > value):
            case(operator == ">=" && this.tables[table].data[i][column] >= value):
            case(operator == "<" && this.tables[table].data[i][column] < value):
            case(operator == "<=" && this.tables[table].data[i][column] <= value):
            case(operator == "like" && this.tables[table].data[i][column].includes(value)):
            case(operator == "LIKE" && this.tables[table].data[i][column].includes(value)):
              match = true;
              break;
          }
        }

        if(match)
        {
          if (filter === null || filter === undefined ||  
              this.tables[table].data[i][filter[0]] == filter[1])
          {
            ret.push(this.tables[table].data[i]);
          }
        }

      }
    }

    if (ret.length >= 1)
    {
      return ret;
    }
    else
    {
      return false;
    }
  }

  sortASC(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  }

  export(ignoreTables)
  {
    /* Turn all tables and their data back into CSV */

    let ret = "";

    Object.keys(this.tables).forEach(function(key, index)
    {

      if (ignoreTables.includes(key) !== true)
      {

        let csv = "";

        for (var j = 0; j < db.tables[key].columns.length; j++)
        {

          csv += db.tables[key].columns[j];
          if (j < (db.tables[key].columns.length - 1))
          {
            csv += ",";
          }

          if (j == (db.tables[key].columns.length - 1))
          {
            csv += "\n";
          }

        }

        for (var j = 0; j < db.tables[key].data.length; j++)
        {

          if(db.tables[key].data[j] !== undefined)
          {

            Object.keys(db.tables[key].data[j]).forEach(function(ky, ind)
            {

                csv += db.tables[key].data[j][ky];
                if (ind < (db.tables[key].columns.length - 1))
                {
                  csv += ",";
                }
              
            });

            csv += "\n";

          }

        }

        ret += key + "\n" + csv + "</>\n";

      }

    });

    return ret;

  }

  import(csv)
  {
    /* Imports an exported database */

    let tables = csv.split("\n</>\n");

    if (tables.length >= 1)
    {

      tables.forEach(function(tbl){

        if (tbl.length >= 1)
        {
          let table = {};
          table.name = tbl.substring(0, tbl.indexOf("\n"));
          tbl = tbl.substring(tbl.indexOf("\n") + 1);
          table.data = tbl;

          db.initTable(table);

        }
      });
    }

    return true;

  }

  commit(ignoreTables)
  {
    /* Commits current database to local storage */

    localStorage.setItem('GSDB', this.export(ignoreTables));

  }

  destroy()
  {
    /* Clears the local storage DB */
    localStorage.removeItem('GSDB');
  }

}